// Array is a single variable that can hold multiple values or other variables. it is stored within a single variable
// [] these brackets indicate an array - within those we have to specify at which position or which index we want to store our value up

let myArray = [];
myArray[0] = 25;
console.log(myArray);
myArray[1] = 35;
myArray[2] = 45;
myArray[3] = true;
myArray[4] = 'hello';
console.log(myArray);

// you can store different things and different values in an array

myArray[2] = false;

// you can update arrays like that

// quicker way to write that array

let array = [25,35,45,'hi',false,true];
console.log(array);

// creating a new keyword

let array2 = new Array(5); // we want to create a new instance of the array object. this way we can specify how many slots you want in the array

console.log(array2); // output: (5) [empty x 5]

// Arrays also have properties and methods(functions that can be used on certain objects)

array2.length;
console.log(array2);

let array3 = [23,'hi',false,true,11];

array3.sort(); // it sorts alphabetically
array3.reverse(); // it sorts the other way round than alphabetically

console.log(array3);


























