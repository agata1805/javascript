// Functions group logical sections of code together, so you can code that section of code whenever you want to
// keyword is function

// function getAverage (a,b){
//
//     let average = (a + b) / 2;
//     console.log(average);
// }
//
// getAverage(7,12); // we have to call the function and you have to give it values

//these parentheses can be empty, but if we're passing variables or values into this function, we just specify these local variables in these brackets

// getAverage(7,8,9); // 9 will be ignored because it's not needed

// Functions can also return a value

function getAverage (a,b){

    let average = (a + b) / 2;
    console.log(average);
    return average;
}
//
// let myResult = getAverage(7,8);
// // We want a new variable called myResult and we're gonna set that variable to equal whatever the result of this is
//
// console.log('the average is ' + myResult);

//
// Variable scope
//

// variable scope determines where in the code a variable is visible and which part of the code can use it
/* Two types of scope:
- local - defined inside of a function, it can only be used within this function, it can't be called later on
- global - has global scope, declared outside of any kind of function at the top level of JS file, you can use it everywhere in the code
 */



function getAverage (a,b){
    let average = (a + b) / 2; // local variable
    return average;
}

// console.log(average); // it will be uncaught reference error cause it's a local variable, it's not defined outside of the function

let myResult = getAverage(7,11); // global variable
console.log('the average is '+ myResult);

function logResult() {
    console.log('the average is '+ myResult + ' inside the function');
}

logResult();

















