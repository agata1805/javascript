//
// traversing the DOM
//

// Get elements by class name

document.getElementsByClassName('content'); // we need to specify which class we want to grab

let myContentDivs = document.getElementsByClassName('content');
console.log(myContentDivs);

// to change the content of the oage:
//1. grab element that you want
// 2. use te property  on that element that you want to change - HTML or the text content of that element
// .innerHTML or .textContent

//
// changing element attributes
//

//1. grab an element you want
//2. use one of two methods: setAttribute or getAttribute

// getAttribute is for finding attribute and getting to know its value
// setAttribute is for changing/updating value of an attribute
let link = document.getElementById('test');
console.log(link);

link.getAttribute('class');
link.setAttribute('class', 'test2'); // choose an attribute you want to change and then change its value
// you can also add a new attribute using setAttribute, do it in the same way as changing the attributw

// you can also do that by using properties
link.className; // it will stay the same
link.className = 'hello'; // it will change a class name to hello

// the behavior of these properties is not always the same as the method getAttribute and setAttribute\

//
// changing CSS styles
//
let nav = document.getElementById('navbar');
console.log(nav);

nav.setAttribute('style', 'position: relative');
// you can do that many times but each time a new style will override the previous one
// you have to put every style in one line so that they don't override themselves
nav.setAttribute('style', 'position: relative; left: 10px');

// another way to do that, more efficient:
nav.style.left = '13px'; // if you want to update just one particular style
nav.style.backgroundColor = 'green'; // no spacing, remember about camelCase


//
// adding elements to the DOM
//
// stages of adding new elements to the Dom:
// 1. create that element in JS
// 2. take that element and push it into the HTML
// to create a new element, you have to store it in a variable

let newLi = document.createElement('li');
console.log(newLi);

let newA = document.createElement('a');
console.log(newA);

// now, that you've created new elements, you have to pop them into HTML

let menu = document.getElementById('main-nav').getElementsByTagName('ul')[0];
console.log(menu);

menu.appendChild(newLi);  // append a new child to this menu item, add a new element
// okay, now there's an li element at the end of the ul. but there's nothing in that li
// we have to APPEND a new child to the li tag. it will be an a tag we created before

newLi.appendChild(newA);
// let us add some content to that a tag

newA.innerHTML = 'blog';
newA.setAttribute('href', 'https://facebook.com');

//if you want to insert an element before some element, you can use insertBefore method

menu.insertBefore(newLi, menu.getElementsByTagName('li')[0]);
// the first value of the method is an element we want to insert and the second is the position

//
// Removing elements from the DOM
//

// 1. grab an element you want to remove and parent element that we want to remove that element from
// 2. Use the removeChild method to remove this child

// you have to store parent and child items in variables
 let parent = document.getElementById('main-nav').getElementsByTagName('ul')[0];
 let child = parent.getElementsByTagName('li')[0];

 console.log(parent);
 console.log(child);

 // first, we call the method on the parent

let removed = parent.removeChild(child);
// it's better to save it in a variable in case you ever want to reinsert that element
// parent.appendChild(removed);





















































