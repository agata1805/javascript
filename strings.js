let myString = 'I am a string'; // that's how we initialize a string
console.log(myString);

// strings are objects and they have methods and properties. one of these properties is length property
// so we can find out the length of any string that you create = the number of characters in that string

console.log(myString.length);

// methods = functions. there are functions that we can perform on a string that are built into a string objects

console.log(myString.toUpperCase());
console.log(myString.toLowerCase());
console.log(myString.indexOf('string')); // we can use it to check if a string has a certain word in it

if (myString.indexOf('ninja') === -1){
    console.log('the word ninja is not in the string');
} else{
    console.log('he word ninja starts at position' + myString.indexOf('nina'));
}

let myString2 = 'I am a ninja string'; // that's how we initialize a string
console.log(myString);

if (myString2.indexOf('ninja') === -1){
    console.log('the word ninja is not in the string');
} else{
    console.log('the word ninja starts at a position' + myString2.indexOf('nina'));
}

let string1 = "abc";
let string2 = 'bcd';

console.log(string1 === string2);

let string3 = "abc";
let string4 = 'abc';

console.log(string3 === string4);

let string5 = 'abc';
let string6 = 'ABC';

console.log(string5 === string6); // it will equal false, cause JS is case sensitive

// you can change that by:

console.log(string5.toLowerCase() === string6.toLowerCase());

//

console.log(string1 < string2); // it's true because a is before b in the alphabet and it's not capital B
// if it was a capital B, it would be false

//
// Splitting and slicing strings
//

// if you want to slice  string
 let str = 'hello world';

 console.log(str);

let str2 = str.slice(2, 9); // - we define from which character to which we want to slice. We start from 0=h and include spaces

console.log(str2);

// when you don't specify a second value for string slice, it cuts you off at the end of the string

let str3 = str.slice(2); // output: "llo world"

// if you want to split string
/*split - a function which takes a string and splits that string up into different pockets
 and puts those pockets in an array
 */

let tags = "meat, pork, salami, chicken, ham, beef";
console.log(tags);

// if we wanted to split those tags so that each meat is in its own pocket in an array, we can do that

let tagsArray = tags.split(','); // we need to specify at what point to split characters and here we choose a comma
console.log(tagsArray);
// whenever you see a comma, I want you to split those things

// it split this variable the string into an array and each position in this array, each pocket is being defined by this thing here
// whenever there is a comma it's splitting this string and putting whatever was before ina new pocket in this array








