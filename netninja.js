// var youLikeMeat = false;
// var myNum = 8;
//
// if (myNum == 10){
//     document.write("myNum is greater than 10");
// } else {
//     document.write('my num is not equal to 10')
// }
//
// if (youLikeMeat){
//     document.write('you like meat');
// } else {
//     document.write('you like me');
// }

//
// Else if statements
//

// let myAge = 9;
//
// if (myAge > 30){
//     document.write('you are over 30');
// } else if (myAge > 20){
//     document.write('you are over 20');
// } else if (myAge > 10){
//     document.write('you are over 10');
// } else {
//     document.write('you are not over 10');
// }

//
// Comparison operators
//
/*
> - bigger
< - smaller
=> bigger or equal
=< smaller or equal
== the same value
=== the same value and type
!= not equal
!=== not the same value and type
 */

let x = 5;
x == 4; // is x equal to 4?
x === 5; // triple equal operator - check the value and type - this is true = value equals 5 and type equals number
//double equal checks just the value


// Negation operator

x != 4 // x is not equal to 4 - true

//
// Logical operators
//

// let myAge = 15;
//
// if (myAge >= 18 && myAge <= 30){ // && = and
//     document.write('i am in');
// } else{
//     document.write('you aint coming');
// }


let myGrade = 14;

if (myGrade < 19 || myGrade > 20){ // || - means OR
    document.write('you passed');
}

// We can have multiple conditions


//
// While loops
//


let age = 5;

// while (condition) {}    // while this condition is true, always execute this code
                        // it will keep looping as long as this condition is true
                        // we have to make sure that at some point this condition is not true so that it doesn't crash your computer

while (age < 10){
    console.log('your age is less than 10');
    age++; // add 1 to age so that your computer doesn't break in an infinite loop
}

document.write('you are now over 10')

//
// For loops
//

/* Loop is meant to repeat certain parts of code for you. A loop will check a certain condition for you and
while that condition is true, it will keep on repeating the code for you
*/

// for (age = 5; age < 10; age++) {
//     console.log('your age is less than 10');
// }

// it's exactly the same as while loop but a little bit easier and neater
//
// let links = document. getElementsByTagName('a');
//
// for (i = 1; i <= links.length; i++){
//
//     console.log("this is link number" + i);
// }
//
// document.write('all links now looped');


//
// Break & continue
//

// for (i = 0; i < 10; i++){
//     if (i === 5 || i === 3){
//         continue;
//     }
//
//     console.log(i);
//
//     if (i === 7){
//         break; // it breaks out the loop
//     }
// }
//
// console.log('I have broken out of the loop');

// We can use the break keyword to break out of the loop at any point we want
// Continue is similar to break in that it will no longer get execute any of the code below this continue word
// Continue: just skip up te code for this i (weaker form of break), Break: breaks out of the loop


//
// Loops practical example
//

// JS is a zero-based language.

var links = document.getElementsByTagName('a');

console.log(links);


for(i = 0; i < links.length; i++){
    links[i].className = "link-" + i;

}

// for as long as i is less than number of links of the page, we'll continue to do this






