//
// Adding events
//

//You can add events JS in HTML but it's not recommended - it makes your HTML messy and clustered

/* Each element that we get in the DOM has these event properties
You grab an element from your code and add an event to it
 */

// 1. get that element and store it in a variable

let title = document.getElementById('page-title');
title.onclick = function () { // it's an anonymous function, we don't need a name cause
        alert('you clicked me'); // we are just assigning this function to this onclick event
};
// that function has been attached to this event

title.onmouseover = function () {
    alert('you hovered your mouse over me');
};

//
// onclick event
//

// event that expands the box on click

// we need to grab those two elements and store them in variables

// let content = document.getElementById('content');
// let button = document.getElementById('show-more');
//
// button.onclick = function(){
//     if(content.className == 'open') {
//         //shrink the box
//         content.className = '';
//         button.innerHTML = 'show more';
//
//     } else{
//         // expand the box
//         content.className = 'open';
//         button.innerHTML = 'show less';
//     }
// };

//
// window onload event
//

// window.onload = function () {
// };
// when that window has fully loaded, then find this function. But until then, do nothing

function setUpEvents() {
    let content = document.getElementById('content');
    let button = document.getElementById('show-more');

    button.onclick = function(){
        if(content.className == 'open') {
            //shrink the box
            content.className = '';
            button.innerHTML = 'show more';

        } else{
            // expand the box
            content.className = 'open';
            button.innerHTML = 'show less';
        }
    };
}

window.onload = function () {
    setUpEvents();
};































