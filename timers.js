//
// let myMessage = document.getElementById('message');
//
// function showMessage() {
//     myMessage.className = 'show';
// }
//
// setTimeout(showMessage, 3000); // 3000ms = 3s, it calls the function once
// // setInterval() // calls the function many times

let colorChanger = document.getElementById('color-changer');
let colors = ['red', 'blue', 'green', 'pink'];
let counter = 0;

function changeColor() {
    if (counter >= colors.length) {
        counter = 0;
    }
    colorChanger.style.backgroundColor = colors[counter];
    counter++;
}

// setInterval(changeColor, 3000);

// to stop an interval or a timer we use clearTimeout or clearInterval

let myTimer = setInterval(changeColor, 3000);

colorChanger.onclick = function () {
    clearInterval(myTimer);
    colorChanger.innerHTML = 'timer stopped';
};











