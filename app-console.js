// window.alert('hello');
// window.alert(2-2);

document.write(12 + 3); // it should be only used for testing

// You should use the console.log() method to print to console JavaScript. The JavaScript console log function is mainly
// used for code debugging as it makes the JavaScript print the output to the console.
// document.write vs console.log
// These methods differ in their purpose and use cases. However, the console.log method is more relevant to the modern
// web development for debugging and logging. The usage of document.write is being discouraged due to the fact that the
// method can override everything in the <header> and <body> elements

//There are three options for changing the content of a specified HTML elements: innerHTML, innerText or textContent.
// When used for output purposes, their functionality is highly similar. However, there is a difference when they are
// applied to get HTML content.

// Let's take a look at what output each of these properties will give when getting the content of the <p> element above.
//
// innerText returns only the text without any of the excess spacing or the <span> tag:
// "This paragraph element has excess spacing and has a span element inside."

// innerHTML returns the text with all of the excess spacing and the <span> tag:
// "   This paragraph element has excess spacing   and has <span>a span element<span>    inside."

// textContent returns the text with all of the excess spacing, but without the <span> tag:
// "   This paragraph element has excess spacing   and has a span element    inside."

console.log('Hello world');