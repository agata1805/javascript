// Log to console
console.log('Hello world');
console.log('123');
console.log('true');
var greeting = 'Hello';
console.log(greeting);
console.log([1,2,3,4]);
console.log({a:1, b:2, c:3});
console.table({a:1, b:2});
console.error('This is some error');
console.clear();
console.warn('This is a warning');
console.time('Hello');
    console.log('Hello world');
    console.log('Hello world');
    console.log('Hello world');
    console.log('Hello world');
console.timeEnd('Hello');

// Variables - var, let and const

// var name = 'John Doe';
// console.log(name);
// name = 'Steven Smith';
// console.log(name); // you can reassign variables using var or let
// var asking;
// console.log(asking); //It will be undefined
// asking = 'Hello';
// console.log(asking);

// Variables can only include letters, numbers, _, $ and can't start with a number. It shouldn't start with an underscore or dollar sign either

// Multi word variables
// var firstName = 'John'; // Camel case
// var first_name = 'Sarah'; // Underscore method
// var FirstName = 'Bruno'; // Pascal case
//
// //Let is very similar to var but it has many advantages when it comes to block level scoping
//
// // let name = 'John Doe';
// // console.log(name);
// // name = 'Steven Smith';
// // console.log(name);
//
// // Const = constant, it means it can't change, it can't be reassigned. And we always have to assign the value
//
// // const name = 'John Doe';
// // console.log(name);
// // name = 'Sara';
//
// // There are two data types: primitive data types and reference data types
// // Primitive data types - stored directly in the location the variable accesses, stored on the stack
// // Reference data types - accessed by reference, objects that are stored on the heap,  pointer to a location in memory
//
// //Primitive data types:
// // string, number, boolean (true or false), null (intentional empty value), undefined, symbols(ES6)
//
// // Reference data types:
// // arrays, object literals, functions, dates, anything else
//
// //JS is a dynamically typed language - this means that the data types are associated with the actual values and not the variables themselves
// // The same variable can hold multiple types - it can hold a string and then it can be set to a number
//
// //PRIMITIVE
// // 1. Strings
// const name = 'John Doe';
// console.log(typeof name); // It will tell us the type of data type
//
// // 2. Number (do not have quotes around them)
// const age = 45;
// console.log(typeof age);
//
// // 3. Boolean
// const hasKids = true;
// console.log(typeof hasKids);
//
// // 4. Null
// const car = null;
// console.log(typeof car); // It says it's an object - in JS world it's kind of a mistake
//
// // 5. Undefined
// let test;
// console.log(typeof test);
//
// // 6. Symbol
// const sym = Symbol();
// console.log(typeof sym);
//
// //REFERENCE - Objects
// // 1. Array
// const hobbies = ['movies', 'music'];
// console.log(typeof hobbies);
//
// // 2. Object literal
// const address = {
//     city: 'Boston',
//     state: 'MA'
// }
// console.log(typeof address);
//
// const today = new Date();
// console.log(today);
// console.log(typeof today);
//
// // Type conversion - take a variable and change a data type e.g. if you're taking input from a form and
// // you're putting that into a variable that's going to be a string by default. And then if you want to
// // apply calculations to it, you're going to want to change it into the number type
//
// let val;
// // Number to string
// val = 5;
//
// // Output
// console.log(val);
// console.log(typeof val);
// console.log(val.length); // Length only works for strings because string math is a string property - it gives you a number of characters in the string
// // Length won't work here - it will be undefined because we're trying to get length from a number
// // If we want to convert it into a string, we can wrap it in the string function
//
// val = String(5);
// console.log(val);
//
// console.log(val);
// console.log(typeof val);
// console.log(val.length); // It will give us 1 for the length, if we were to put "555" it would give us 3
//
// val = String(4+4);
//
// console.log(val); // 8 for the value
// console.log(typeof val); // type: string
// console.log(val.length) // length = 1
//
// // Boolean to a string
//
// val = String(true);
//
// console.log(val); // true
// console.log(typeof val);
// console.log(val.length); // 4
//
// // Date to a string
//
// val = String(new Date());
//
// console.log(val);
// console.log(typeof val);
// console.log(val.length);
//
// // Array to a string
//  val = String([ 1,2,3,4,5,6]);
//
// console.log(val);
// console.log(typeof val);
// console.log(val.length);
//
// // toString method
//
// val = (5).toString();
//
// console.log(val);
// console.log(typeof val);
// console.log(val.length);
//
// val = (true).toString();
//
// console.log(val);
// console.log(typeof val);
// console.log(val.length);
//
// // String to number
//
// val = '5';
//
// console.log(val);
// console.log(typeof val);
// console.log(val.length); // It only works on strings
//
//
// val = Number('5');
//
//
// console.log(val);
// console.log(typeof val);
// console.log(val.toFixed(2)); // It only works on numbers and allows us to specify decimals - liczby dziesiętne

// Boolean to a number

// val = Number(true);
//
// console.log(val); // true value as a number gives us 1
// console.log(typeof val);
// console.log(val.toFixed(2));
//
// val = Number(false);
//
// console.log(val); // false value as a number gives us 0
// console.log(typeof val);
// console.log(val.toFixed(2));
//
//
// val = Number(null);
//
// console.log(val); // its value as a number gives us 1
// console.log(typeof val);
// console.log(val.toFixed(2));
//
// val = Number('hello');
//
// console.log(val); // It gives us NaN - not a number - this is what happens when value tries to get parses a number but can't
// console.log(typeof val);
// console.log(val.toFixed(2));

// Array to number

// val = Number([1,2,3]);
//
// console.log(val); // the same - NaN
// console.log(typeof val);
// console.log(val.toFixed(2));
//
// val = parseInt('100');
//
// console.log(val); // it will give 100 as a number
// console.log(typeof val);
// console.log(val.toFixed(2));
//
//
// val = parseInt('100.30');
//
// console.log(val); // it still will give 100 as a number, it's gonna pass it as an integar
// console.log(typeof val);
// console.log(val.toFixed(2));
//
// //If we want to add decimals, we need to use something called parseFloat
//
// val = parseFloat([100.30]);
//
// console.log(val); //
// console.log(typeof val);
// console.log(val.toFixed(2));

// Type coersion

// const val1 = 5;
// const val2 = 6;

// const sum = val1 + val2;
//
// console.log(sum); // 11, obviously
// console.log(typeof sum); // number

// const val1 = String(5);
// const val2 = 6;

// const sum = val1 + val2;

// console.log(sum); // 56 - js engine saw that we have 2 values that we're adding together and it took it upon
// // itself to change one to a string and add it. It changed 6 to a string and concatenated it, put one in front
// // of the other, so it made 56 - 56 is a string
// console.log(typeof sum);

// const sum = Number(val1 + val2);

//
// Lesson 10 - Numbers & the math object
//

// const num1 = 100;
// const num2 = 50;
//
// let val;

// Simple math with numbers

// val = num1 + num2;
// val = num1 * num2;
// val = num1 - num2;
// val = num1 / num2;
// val = num1 % num2; // we'll get 0 - modulus operator - it'll give us what's left over as a remainder
//
// console.log(val);

// Math object

// val = Math.PI;

// Math is an object, meaning it has properties and methods
// A property is basically ike an attribute and then a method is just a function inside of an object
// PI is gonna give us number PI = 3.14...

// val = Math.E;
// val = Math.round(2.4); // If you want to round a number
//
// val = Math.ceil(2.4); // If you want to round up
// val = Math.floor(2.6) // If you want to round down
//
// val = Math.sqrt(64); // If we want to square something // we get 8 here
// val = Math.abs(-3); // It will give us an absolute number - 3
//
// val = Math.pow(8, 2); // it shows power
// val = Math.min(2,33,4,1,55,6,3); // it shows the minimal value
// val = Math.max(2,4,5,123,4,5,678);
// val = Math.random(); // Gives a random decimal
//
// val = Math.random() * 20; // When you want a whole number not just a decimal // 20 is the max number you want
// val = Math.random() * 20 +1; // random whole number from 1 to 20 with decimals
// val = Math.floor(Math.random() * 20 +1); // a random whole number from 1 to 20 without decimals
//
//
// console.log(val);


//
// Lesson 11 - Strings and Concatenation
//
//
// const firstName = 'William';
// const lastName = 'Johnson';
// // const age = 36;
// const str = 'Hello there my name is Brad'
//
// console.log(firstName);
// console.log(lastName);
// // console.log(age);
//
// let val;
//
// val = firstName + lastName; // result will be 'WilliamJohnson'
//
//
// // Concatenation
//
// val = firstName + ' ' + lastName;
//
// // Appending - adding to a variable
//
// val = 'Brad';
// val = 'Traversy'; // it's gonna override the previous one
//
// val = 'Brad ';
// val += 'Traversy'; // output will be 'Brad Traversy'
//
// // val = 'Hello, my name is ' + firstName + ' and I am ' + age;
//
// // Escaping
//
// val = "That's awesome, I can't wait"; // or
// val = 'That\'s awesome, I can\'t wait';
// // this backslash escapes this code, making it just a regular character, it takes its power away
//
// // Length
//
// val = firstName.length; // it's a property and not a method so we don't need parentheses
// // counts the number of characters in the string
//
// // Concat
//
// val = firstName.concat(' ', lastName);
//
// // Change case
//
// val = firstName.toUpperCase();
// val = firstName.toLowerCase();
//
// val = firstName[0] // it will give us 'W' - zero index - name William starts with W which is 0 in arrays
//
// // index of method
//
// val = firstName.indexOf('l'); // it looks for the first 'l' in the name string
// val = firstName.lastIndexOf('l'); // it looks for the last 'l' in the string
// // When you're using indexof and the character you're looking for is not there, then it equals -1
//
// // charAt method
//
// val = firstName.charAt('2');
//
// // Get last character if the string
//
// val = firstName.charAt(firstName.length - 1);
//
// // Get substrings - pull a substring out of a string
//
// val = firstName.substring(0,4); // We'll get 'Will', a string from 0 to 4, strings start with 0
//
// // Get slices of a string
//
// val = firstName.slice(0,4); // it's very similar to substring but we can also put negative numbers into it
//
// val = firstName.slice(-3); // output: iam - the last three letters of the name 'William'
//
// // Split - it can split a string into an array based on a separator
//
// // you can turn it into an array based on spaces
//
// val = str.split(' ') // output: "hello", "there", "my" ... it's a 6 value array and each word is in each index here
//
//
// // Replace
//
// val = str.replace('Brad', 'Jack');
//
// // Includes
//
// val = str.includes('Hello'); // it will be true because 'hello' is included in the val

//
// Lesson 12 - Template literals
//

const name = 'John';
const age = 30;
const job = 'Web Developer';
const city = 'Miami';

console.log(name);
console.log(age);
console.log(job);
console.log(city);

// Without template strings

let html;

html = '<ul><li>Name: ' + name + '</li><li>Age: ' + age + '</li><li>Job: ' + job + '</li><li>City: ' + city + ' </li></ul>';
// An HTML string

console.log(html);

html = '<ul>' +
        '<li>Name: ' + name + '</li>' +
        '<li>Age: ' + age + '</li>' +
        '<li>Job: ' + job + '</li>' +
        '<li>City: ' + city + '</li>' +
        '</ul>';


// With template strings/literals

function hiya(){
    return 'hiya';
}

html = `
    <ul>
        <li>Name: ${name}</li>
        <li>Age: ${age}</li>
        <li>Job: ${job}</li>
        <li>City: ${city}</li>
        <li>${2 + 2}</li>
        <li>${hiya}</li>
    </ul>
`;


console.log(html);

// document.body.innerHTML = html;

var youLikeMeat = true;

console.log(youLikeMeat);

if (youLikeMeat) {
    document.write('Here is the meaty area...');
}














