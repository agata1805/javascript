# Introduction
***
This repository is a documentation of what I've done throughout the 1/3 of Brad Traversy course called Modern JavaScript from the beginning course and The Net Ninja course called Modern JavaScript. 

# Table of contents 
***
To better navigate through that repository, it is essential to provide its table of contents. Most of section have two files: HTML and JS file.
1. About JavaScript
2. What is a console and why you should use it
3. Introduction to arrays
4. JS events
5. JS functions
6. Numbers
7. Objects
8. Strings
9. JS timers
10. Variables

# Repository purpose
***
I used this repository and the files I created to:
- familiarize myself with basic concepts from JavaScript
- refresh my knowldedge of git and git commands
- get to know how to work in console

