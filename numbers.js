let myNumber = 3; // this is a number
let myNumberString = '1.23445'; // this is a string

let a = 5;
let b = 5;

console.log(a + b); // we get 10, gotcha

let c = '5';

console.log(c + b); /* we get 55, two things are going on - concatenation (squashing the two things together),
it's not adding them, it's putting them side by side. The second thing is - it's taking that number
and it's turning it into a string, 55 is no longer a number (it's not blue in the console)
*/

console.log(typeof (a + b)); // number
console.log(typeof (b + c)); // string

//
// Math object
//

Math.round(7.5); // rounds a number

console.log(Math.round(7.5)); // gives us 8

Math.floor(7.9); // rounds down no matter what the number
Math.ceil(7.1); // rounds up no matter what the number
console.log(Math.max(7, 4, 8, 12)); // gonna give us a maximum number
// It also logs constants like PI
console.log(Math.PI);

//
// Nan - no a number
//

// checking if something is a number

let e = 'apple';
let f = 5;

if (isNaN(e)){
    console.log('that aint even a number!');
} else{
    console.log('meaning of life is ' + (e * f));
}

let g = 7;
let h = 5;

if (isNaN(g)){
    console.log('that aint even a number!');
} else{
    console.log('meaning of life is ' + (g * h));
}

// ! - negator operator

// double negative

let j = 7;
let k = 5;

if (!isNaN(j)){
    console.log('that aint even a number!');
} else{
    console.log('meaning of life is ' + (j * k));
}








