// Objects: string, number, array and other different things in JS

// What is an object?
// Object - a container which encloses data and behavior together when they belong together
/* e.g. - you make an array which is an object. and on that array you can extract data such as the length property
and extract behavior such as the sort function when you can sort the array into a particular order
*/

// objects have their properties and behaviors. and in programming they are called methods of the object.
//  we can call properties on it using the dot notation


//
// Create a new object in JS
//

// let myArray = new Array();
// myArray[0] = 8;
// myArray[1] = 'hello';
//
// let myCar = new Object();
//
// myCar.maxSpeed = 50;
// myCar.driver = 'Shaun'; // properties
// myCar.drive = function () {
//     console.log('now driving');
// }
//
// console.log(myCar);
// myCar.drive(); // calling function to action
//
// let myCar2 = {
//     maxSpeed: 70,
//     driver: 'Net Ninja',
//     drive: function () {
//         console.log('now driving again');
//     }
// }; // shorthand notation for creating an object
//
// console.log(myCar2.maxSpeed);
// console.log(myCar2.driver);
// myCar2.drive();
//
// let myCar3 = {
//     maxSpeed: 70,
//     driver: 'Net Ninja',
//     drive: function (speed, time) {
//         console.log(speed * time);
//     }
// };
//
// console.log(myCar3.maxSpeed);
// console.log(myCar3.driver);
// myCar3.drive(70, 3);
//
// //
// // THIS keyword
// //
//
// // this keyword refers to whatever object currently owns that space that you use this keyword in
//
// console.log(this); // it will refer to the window object
//
// let car = {
//     maxSpeed: 80,
//     driver: "Agata",
//     drive: function (speed, time) {
//         console.log(speed * time);
//     },
//     test: function () {
//         console.log(this);
//     }
// };
//
// console.log(car.maxSpeed);
// console.log(car.driver);
// car.drive(45, 2);
//
// //
// // Constructor functions
// //
//
// // a constructor function is a function that creates an object for us
//
// let Car = function (maxSpeed, driver) {
//     this.maxSpeed = maxSpeed;
//     this.driver = driver;
//     this.drive = function (speed, time) {
//         console.log(speed * time);
//     };
//     this.logDriver = function () {
//         console.log('driver name is ' + this.driver);
//     };
//
// }

// constructor functions usually start with a capital letter


let myCar1 = {
    maxSpeed: 70,
    driver: 'net ninja',
    drive: function (speed, time) {
        console.log(speed * time);
    },
    logDriver: function () {
        console.log('driver name is ' + this.driver);
    }
};

let Car = function (maxSpeed, driver) {
    this.maxSpeed = maxSpeed;
    this.driver = driver;
    this.drive = function (speed, time) {
        console.log(speed * time);
    };
    this.logDriver = function () {
        console.log('driver name is ' + this.driver);
    };
}

let myCar2 =  new Car(70, 'Agata driver');
let myCar3 =  new Car(2, 'ninja driver');
let myCar4 =  new Car(34, 'baby driver');
let myCar5 =  new Car(90, 'ninja driver');
let myCar6 =  new Car(70, 'ninja driver');
let myCar7 =  new Car(70, 'ninja driver');


myCar2.drive(30,5);
myCar3.logDriver();

//
// Date object
//

// date object - is a JS in-built object that can be used to create dynamic dates or store previews of future dates
// first thing we do = we store our dates in variables

let myDate = new Date(); // it's gonna store the current date in this variable
console.log(myDate); // it;s dynamic, you don't have to keep editing yourself

let myPastDate = new Date(1545, 11, 2, 10, 30, 12);
let myFutureDate = new Date(2515, 0, 31);

console.log(myPastDate);
console.log(myFutureDate);


let birthday = new Date( 1985, 0, 15, 11, 15, 25);
let birthday2 = new Date( 1985, 0, 15, 11, 15, 25);

 // we can call different methods on that birthday date
// the first one is get month - it gives us back the month in the form of number from 0 to 11

//get the month of the date (0 - 11)
console.log(birthday.getMonth());

//get the full year (YYYY)
console.log(birthday.getFullYear());

//get the date of the month (1 - 31)
console.log(birthday.getDate());

//get the day of the week (0 - 6)
console.log(birthday.getDay());

//get the hour of the date (0 - 23)
console.log(birthday.getHours());

//get the number of milliseconds since 1st January 1970
console.log(birthday.getTime());

if (birthday == birthday2){
    console.log('birthdays are equal');
} else {
    console.log('birthdays are not equal');
}
// they won't be equal cause js doesn't work like that
// but if you set gettime method, they will be equal

if (birthday.getTime() == birthday2.getTime()){
    console.log('birthdays are equal');
} else {
    console.log('birthdays are not equal');
}

























































