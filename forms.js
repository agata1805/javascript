let myForm = document.forms.myForm; // we need a name of the form

console.log(myForm);
myForm.name.value;

myForm.name.onfocus = function () {
    myForm.name.style.border = '4px solid pink';
};

myForm.name.onblur = function () {
    myForm.name.style.border = 'none';
};

//
// form validation
//

let message = document.getElementById('message');

//onSubmit event occurs when you click submit

myForm.onsubmit = function () {
    if (myForm.name.value == ''){
        message.innerHTML = 'please enter a name';
        return false;
    } else{
        message.innerHTML = '';
        return true;
    }
};




